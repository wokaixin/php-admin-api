<?php
namespace app\admin\model;

class Ad extends \app\common\model\Ad {

	// 关联图片模型
	public function adCategory() {
		// return $this->belongsTo('newsImage', 't_id', 'id', ['newsImage' => 'a', 'news' => 'c'], 'LEFT');
		return $this->hasOne('adCategory', 'id', 'category_id');
		// ->field(['id', 't_id', 'src'])
	}
	public function area() {
		return $this->hasOne('area', 'id', 'area_id');
	}
}