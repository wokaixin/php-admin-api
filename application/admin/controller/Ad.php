<?php
namespace app\admin\controller;
use app\admin\model\Ad as Mod;
use app\admin\model\AdImage as AdImageModel;
// use app\home\model\User as UserModel;
use app\admin\validate\Ad as ModValidate;
use yichenthink\utils\FileBase64;
use yichenthink\utils\ReturnMsg;

class Ad extends Base {
	// 查询列表

	public function list($category_id = 0, $isTrashed = 0, $area_code = 0) {
		$message = '没有数据';
		$code = 400;

		$map = [];
		if (!empty($category_id) && is_numeric($category_id)) {
			$map[] = ['category_id', '=', $category_id];
		}
		if (!empty($area_code) && is_numeric($area_code)) {
			$area_code = rtrim($area_code, '0');
			$map[] = ['area_code', 'like', $area_code . '%'];
		}
		if ($isTrashed == 1) {
			$data = Mod::onlyTrashed()->alias('a')->where($map)->leftjoin('ad_category b', 'b.id=a.category_id')->leftjoin('area c', 'c.id=a.area_code')->field(['a.id id', 'a.title title', 'a.url url', 'a.image image', 'a.create_time create_time', 'a.update_time update_time', 'a.state state', 'a.category_id category_id', 'b.genealogy genealogy', 'b.name categoryName'])->select();
		} else {
			$data = Mod::alias('a')->where($map)->leftjoin('ad_category b', 'b.id=a.category_id')->leftjoin('area c', 'c.id=a.area_code')->field(['a.id id', 'a.longitude longitude', 'a.latitude latitude', 'a.area_code area_code', 'a.flux flux', 'a.title title', 'a.url url', 'a.image image', 'a.create_time create_time', 'a.update_time update_time', 'a.state state', 'a.category_id category_id', 'b.genealogy genealogy', 'b.name categoryName', 'c.name areaName'])->select();
		}

		if ($data) {
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	public function detail($id = '0') {
		$message = '没有数据';
		$code = 400;

		$data = Mod::alias('a')->where('a.id', $id)->leftjoin('ad_category b', 'b.id=a.category_id')->leftjoin('area c', 'c.id=a.area_code')->field(['a.id id', 'a.longitude longitude', 'a.latitude latitude', 'a.area_code area_code', 'a.flux flux', 'a.title title', 'a.url url', 'a.image image', 'a.create_time create_time', 'a.update_time update_time', 'a.state state', 'a.category_id category_id', 'b.genealogy genealogy', 'b.name categoryName', 'c.name areaName'])->find();

		if ($data) {
			$code = 200;

			// $message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}
	public function update($id) {
		$form = $this->param;
		$form['user_id'] = $this->userInfo['uid'];
		//设置允许修改的字段
		$message = '';
		$code = 400;
		// ReturnMsg::returnMsg($code, $message, $form);
		$map = [["id", '=', $id]];
		// ReturnMsg::returnMsg($code, $message, $form);
		// $map[] = ['user_id', '=', $this->userInfo['uid']];
		// 过滤数组中的安全字段
		$validate = new ModValidate;

		if (!$validate->check($form)) {

			ReturnMsg::returnMsg($code, $validate->getError(), $form);
		}

		$Mod = Mod::where($map)->find();
		$isUpImg = false;
		if (isset($form['image']) && $Mod->image != $form['image']) {
			$FileBase64 = new FileBase64;
			//获取base64流的信息
			if ($FileBase64->getBase64Info($form['image'])) {
				// 预备上传 返回图片上传后的信息 路径名 图片类型，尺寸等信息
				$FileBase64->readyUpload('ad/' . date('y/m/d/'));
				//获取图片地址
				$form['image'] = $FileBase64->url;
				$FileBase64->destroy($Mod->image);
				$isUpImg = true;
			} else {
				$form['image'] = $Mod->image;
			};
		}

		if ($Mod && $Mod->allowField(['state', 'user_id', 'image', 'title', 'category_id', 'url', 'area_code'])->save($form)) {
			if ($isUpImg) {
				$FileBase64->upload();
			}
			$code = 200;

		}

		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	public function add() {
		$form = $this->param;
		$form['user_id'] = $this->userInfo['uid'];
		//设置允许修改的字段
		$message = '';
		$code = 400;
		// ReturnMsg::returnMsg($code, $message, $form);

		// 过滤数组中的安全字段
		$validate = new ModValidate;

		if (!$validate->check($form)) {

			ReturnMsg::returnMsg($code, $validate->getError(), $form);
		}

		$Mod = new Mod;
		$isUpImg = false;
		if (isset($form['image'])) {
			$FileBase64 = new FileBase64;
			//获取base64流的信息
			if ($FileBase64->getBase64Info($form['image'])) {
				// 预备上传 返回图片上传后的信息 路径名 图片类型，尺寸等信息
				$FileBase64->readyUpload('ad/' . date('y/m/d/'));
				//获取图片地址
				$form['image'] = $FileBase64->url;
				$isUpImg = true;
			}
		}

		if ($Mod && $Mod->allowField(['state', 'user_id', 'image', 'title', 'category_id', 'url', 'area_code'])->save($form)) {
			if ($isUpImg) {
				$FileBase64->upload();
			}
			$code = 200;

		}

		ReturnMsg::returnMsg($code, $message, $Mod);
	}
	// 删除一条
	public function delete($id = 0, $isTrue = false) {
		$map[] = ['id', '=', $id];
		$map[] = ['user_id', '=', $this->userInfo['uid']];
		$code = 400;
		$Mod = Mod::withTrashed()->where($map)
			->find();
		if ($Mod) {
			if ($isTrue) {
				// 取出图片
				if (isset($Mod->image)) {
					$FileBase64 = new FileBase64;
					// 删除图片资源
					$FileBase64->destroy($url = $Mod->image);
				}

				$Mod->delete($isTrue);
				$delete = new \app\home\model\DeleteTable;
				// 过滤数组中的安全字段
				$safe = [
					'state' => 'state',
					'user_id' => 'user_id',
					'category_id' => 'category_id',
					'content' => 'content',
					'title' => 'title',
					'id' => 't_id',
				];

				$delete->t_id = $Mod->id;
				$delete->model = __CLASS__;
				$delete->func = __FUNCTION__;
				// $delete->content = $Mod->content;
				$delete->title = $Mod->title;
				$delete->sql = $Mod->getLastSql();
				$delete->allowField(['title', 'content', 'sql', 'model', 't_id', 'func'])->save();
				# code...
			} else {
				$Mod->delete($isTrue);
			}

		}
		if (0 !== $Mod) {
			$code = 200;
		}

		ReturnMsg::returnMsg($code, '', $Mod);
	}

	//恢复一条
	public function restore($id = 0) {

		$map = [];
		$map[] = ['id', '=', $id];
		$code = 400;
		$Mod = Mod::onlyTrashed()
			->where($map)
			->find();
		# code...
		if ($Mod) {
			$code = 200;
			$Mod->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code, '', $Mod);
	}

}
