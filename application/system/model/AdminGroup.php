<?php
namespace app\system\model;

class AdminGroup extends \app\common\model\AdminGroup {

	public function adminFunc() {
		return $this->hasMany('admin_func', 'group_id', 'id');
	}
	public function adminUser() {
		return $this->hasMany('adminUser', 'rank_id', 'id');
	}
	// 关联职位模型
	public function AdminGroupFunc() {
		return $this->hasMany('admin_group_func', 'group_id', 'id');
		// return $this->hasOne('admin_func', 'id', 'admin_rank_func.func_id');
	}
}