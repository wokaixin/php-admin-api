<?php
namespace app\system\controller;
use app\system\model\AdminFunc as AdminFuncModel;
use app\system\model\AdminGroup as Mod;
use app\system\model\AdminUser as AdminUserModel;
use yichenthink\utils\ReturnMsg;

class AdminGroup extends AdminBase {

	// 管理员权限 查询所有职务权限列表
	public function list($p = 0, $trashed = null) {

		$num = 100;
		$code = 400;
		$message = '';
		$startNum = $num * $p;
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->with(['AdminGroupFunc.adminFunc'])
				->limit($startNum, $num)
				->visible(['id', 'group_name', 'create_time', 'update_time', 'group_id', 'admin_func', 'admin_group_func'])
				->select();
		} else {
			$data = Mod::with(['AdminGroupFunc.adminFunc'])
				->limit($startNum, $num)
				->visible(['id', 'group_name', 'create_time', 'update_time', 'group_id', 'admin_func', 'admin_group_func'])
				->select();
		}
		// $data->visible(['admin_group_func' => ['id', 'func_name']])->toArray();
		if ($data != Null) {
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $data->visible(['admin_group_func' => ['id', 'func_name', 'func_id']])->toArray());
	}
	// 系统管理员无限制的查询全部功能列表
	public function funcList($p = 0, $trashed = null) {
		$num = 100;
		$code = 400;
		$message = '';
		$startNum = $num * $p;
		$data = AdminFuncModel::limit($startNum, $num)
			->visible(['id', 'func_name', 'action'])
			->select();
		if ($data != Null) {
			$code = 200;
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}

	// 增加一条新数据
	public function add($group_name) {
		//设置允许修改的字段
		$message = '';
		$code = 400;
		$userInfo = $this->userInfo;
		$map = [];
		$map[] = ['user_id', '=', $userInfo['uid']];
		$map[] = ['group_name', '=', $group_name];
		$R = Mod::where($map)->find();
		if (Null == $R) {
			$Mod = new Mod;
			$Mod->user_id = $userInfo['uid'];
			$Mod->group_name = $group_name;
			$Mod->save();
			$map[] = ['id', '=', $Mod->id];
			if (false !== $R) {
				$code = 200;
				// $Mod::where($map)
				// ->visible(['id', 'group_name', 'create_time', 'update_time'])
				// ->find();
				ReturnMsg::returnMsg($code, $message, $Mod->visible(['id', 'group_name', 'create_time', 'update_time']));
			}

		} else {

			$message = '职务已存在';
		}
		ReturnMsg::returnMsg($code, $message);
	}
	// 删除一条职务
	public function delete($id = 0, $isTrue = false) {

		$user = Mod::withTrashed()->where('id', $id)
			->find();
		$user->delete($isTrue);
		if (0 !== $user) {
			$code = 200;
		} else {
			$code = 400;
		}

		ReturnMsg::returnMsg($code, '', $user);
	}

	//恢复一条职务
	public function restore($id = 0) {
		$map = [];
		$map[] = ['id', '=', $id];
		$code = 400;
		$Mod = Mod::onlyTrashed()
			->where($map)
			->find();
		# code...
		if ($Mod) {
			$code = 200;
			$Mod->restore();
		}
		ReturnMsg::returnMsg($code);
	}
	// 修改职务名称
	public function update($id, $group_name) {

		//设置允许修改的字段
		$message = '';
		$map = [];
		$map[] = ['id', '=', $id];
		$map[] = ['update_user_id', '=', $this->userInfo['uid']];
		$code = 400;
		$Mod = Mod::where($map)->find();

		if (null != $Mod) {
			$Mod->group_name = $group_name;
			$R = $Mod->save();
			if (false !== $R) {

				$code = 200;
			} else {
				$message = '操作失败';
				$code = 400;
			}
		} else {
			$message = '职位不存在';
			$code = 400;
		}
		ReturnMsg::returnMsg($code, $message);
	}

}
