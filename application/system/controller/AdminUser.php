<?php
namespace app\system\controller;
use app\system\model\Admin as AdminModel;
use app\system\model\AdminUser as Mod;
use app\system\model\User as UserModel;
use yichenthink\utils\ReturnMsg;

class AdminUser extends AdminBase {

// 管理员权限查询团队列表
	public function list($trashed = null) {
		$message = '';
		$code = 400;
		// 查询被删除的
		if ($trashed) {
			$data = Mod::onlyTrashed()
				->with(['adminRank'])
				->visible(['user_id', 'user_name', 'rank_name', 'create_time', 'update_time', 'admin_id'])
				->select();
		} else {
			// 查询未删除的
			$data = Mod::with(['adminRank'])
				->visible(['user_id', 'user_name', 'rank_name', 'create_time', 'update_time', 'admin_id'])
				->select();
		}
		if (isset($data[0])) {
			$code = 200;
			$message = '成功';
		} else {
			$message = '没找到数据';
		}
		ReturnMsg::returnMsg($code, $message, $data);
	}

	// 给团队增加一个成员
	public function add($user_id) {

		$message = '';
		$code = 400;
		$R = UserModel::where('id', $user_id)->find();
		if (Null != $R) {
			$Mod = new Mod;
			$uid = $Mod->where('user_id', $user_id)->find();
			if ($uid == Null) {
				$Mod->user_id = $user_id;
				$Mod->admin_id = $this->userInfo['uid'];
				$Mod->user_name = $R->nickname;
				$RB = $Mod->save();
				if (false !== $RB) {
					$code = 200;
					$data = $Mod::withTrashed()
						->where('user_id', $user_id)
						->with(['user', 'adminRank'])
						->visible(['user_id', 'user_name', 'create_time', 'update_time', 'rank_id', 'rank_name'])
						->find();
					ReturnMsg::returnMsg($code, $message, $data);
				}
			} else {
				$message = '用户已在管理列表，无需重复添加';
			}
		} else {

			$message = '用户不存在';
		}
		ReturnMsg::returnMsg($code, $message);
	}
	// 修改自己名字
	public function MyUpdate($user_name) {
		//设置允许修改的字段
		$message = '';
		$Mod = new Mod;
		$user = $Mod->where('user_id', $this->userInfo['uid'])->find();
		if ($user != Null && $user->user_name != $user_name) {
			$user->user_name = $user_name;
			if ($user->save() != Null) {
				$code = 200;
			}
		} else {
			$message = '操作失败';
			$code = 400;
		}

		ReturnMsg::returnMsg($code, $message, $user);
	}
	// 修改成员职务权限或名字
	public function update($id, $form = []) {
		//设置允许修改的字段
		$upfield = ['rank_id' => 'rank_id', 'user_name' => 'user_name'];
		$message = '';
		$Mod = new Mod;
		$user = $Mod->where('user_id', $id)->find();

		if (null != $user) {
			foreach ($upfield as $key => $value) {
				# code...
				if (isset($form[$key]) && !empty($form[$key])) {
					# code...
					$user->$value = $form[$key];
				}
			}
			$R = $user->save();
			if (false !== $R) {

				$code = 200;
			} else {
				$message = '操作失败';
				$code = 400;
			}
		} else {
			$message = '用户不存在';
			$code = 400;
		}
		ReturnMsg::returnMsg($code, $message, $user);
	}
	// 删除一个成员
	public function delete($user_id = 0, $isTrue = false) {

		$user = Mod::withTrashed()
			->where('user_id', $user_id)
			->find();
		$user->delete($isTrue);
		if (0 !== $user) {
			$code = 200;
		} else {
			$code = 400;
		}

		ReturnMsg::returnMsg($code, '', $user);
	}

	//恢复一个成员
	public function restore($user_id = 0) {
		$num = 0;
		$user = Mod::onlyTrashed()
			->where('user_id', $user_id)
			->find();
		# code...
		if ($user) {
			$num++;
			# code...
			$user->restore();
		}
		// $user = UserModel::restore($list);
		ReturnMsg::returnMsg($code = 200, '成功恢复了' . $num . '条数据');
	}

}
