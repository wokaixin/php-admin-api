<?php
namespace app\home\controller;
use app\home\model\News as Mod;
// use app\home\model\User as UserModel;
use app\home\model\NewsImage as NewsImageModel;
use yichenthink\utils\ReturnMsg;

class News extends Base {
	// 查询系统管理人员列表

	public function list($category_id = 1) {
		$message = '没有数据';
		$code = 400;
		$map = [];
		if (isset($category_id) && !empty($category_id)) {
			$map[] = ['category_id', '=', $category_id];
		}
		// 查询未删除的
		$data = Mod::with(['newsContent' => function ($query) {
			$query->field(['content', 'image', 'title', 't_id']);
		}])->where($map)->select();
		if ($data) {
			$count = count($data);
			for ($i = 0; $i < $count; $i++) {
				$data[$i]->abstract = mb_substr($data[$i]->content, 0, 100);
			}
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data->visible(['id', 'title', 'create_time', 'update_time', 'abstract', 'image']));
	}
	public function detail($id = '0') {
		$message = '没有数据';
		$code = 400;

		$list = Mod::field('id')->where(['id' => $id])->with(['newsImage' => function ($query) {
			$query->field(['src', 't_id'])->visible(['src']);
		}, 'newsContent' => function ($query) {
			$query->field(['content', 'image', 'title', 't_id']);
		}])->find();

		if ($list) {
			$code = 200;

			// $message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $list);
	}

}
