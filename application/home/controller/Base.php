<?php
namespace app\home\controller;

use think\facade\Cache;
use think\facade\Request;
use yichenthink\utils\ReturnMsg;
use yichenthink\utils\Url;

class Base extends \app\Base {
	protected $inlet = "/home.php"; //放行的支持入口文件
	protected $param = []; //请求数据 如果设置了拦截过滤请求数据,请使用该属性来获取过滤后的数据

	protected function initialize() {

		// 仅支持admin.php入口进行访问。
		$path = Request::server()['SCRIPT_NAME'];
		if (substr_compare($path, $this->inlet, -strlen($this->inlet)) !== 0) {
			ReturnMsg::returnMsg(400, "非法请求,请使用，http://" . Request::host() . $this->inlet . "进行访问");
		}
		// Cache::clear(); //清空缓存用于测试时候实时
		// 继承父类
		parent::initialize();
		// 获取当前地址栏不含域名后缀和参数 用0替换斜杠
		$this->path = Url::replaceStr(Request::path());
		// $this->path = Request::path();
		// $this->safeUrl();
	}

}
