<?php
namespace app\home\controller;
use app\home\model\Nav as Mod;
// use app\home\model\User as UserModel;
use app\home\model\NewsImage as NewsImageModel;
use yichenthink\utils\ReturnMsg;

class Nav extends Base {
	// 查询系统分类列表

	public function list() {
		$message = '没有数据';
		$code = 400;
		// 查询未删除的
		$data = Mod::where(['state' => '1'])->select();
		if ($data) {
			$code = 200;
			$message = '成功';
		}
		ReturnMsg::returnMsg($code, $message, $data->visible(['id', 'ranking', 'name', 'image', 'url', 'pid']));
	}
	// public function detail($id = '0') {
	// 	$message = '没有数据';
	// 	$code = 400;

	// 	$list = Mod::field('id')->where(['id' => $id])->with(['newsImage' => function ($query) {
	// 		$query->field(['src', 't_id'])->visible(['src']);
	// 	}, 'newsContent' => function ($query) {
	// 		$query->field(['content', 'image', 'title', 't_id']);
	// 	}])->find();

	// 	if ($list) {
	// 		$code = 200;

	// 		// $message = '成功';
	// 	}
	// 	ReturnMsg::returnMsg($code, $message, $list);
	// }

}
