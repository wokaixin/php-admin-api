<?php
namespace app\common\model;

class DeleteTable extends Model {
	// 定义时间戳字段名
	protected $createTime = 'create_time';
	protected $autoWriteTimestamp = true;
	// 定义默认值
	protected $insert = ['state' => 1];

	// 关联内容模型
	public function newsContent() {
		return $this->hasOne('NewsContent', 't_id', 't_id')->bind([
			'title' => 'title',
			'content' => 'content',
			'image' => 'image',
		]);
	}
}